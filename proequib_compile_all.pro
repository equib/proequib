.compile pro/calc_abundance
.compile pro/calc_temperature
.compile pro/calc_density
.compile pro/calc_emissivity
.compile pro/calc_populations

.compile pro/gamma_hb_4861
.compile pro/gamma_he_ii_4686
.compile pro/find_aeff_sh95_column

.compile pro/calc_abund_he_i_rl
.compile pro/calc_abund_he_ii_rl
.compile pro/calc_abund_c_ii_rl
.compile pro/calc_abund_c_iii_rl
.compile pro/calc_abund_o_ii_rl
.compile pro/calc_abund_n_ii_rl
.compile pro/calc_abund_n_iii_rl
.compile pro/calc_abund_ne_ii_rl

.compile pro/deredden_flux
.compile pro/deredden_relflux
.compile pro/redlaw
.compile pro/redlaw_gal
.compile pro/redlaw_gal2
.compile pro/redlaw_smc
.compile pro/redlaw_lmc
.compile pro/redlaw_ccm
.compile pro/redlaw_fm
.compile pro/redlaw_jbk

.compile pro/lin_interp

.compile externals/misc/_interp_2d
.compile externals/misc/_sign
.compile externals/misc/_interp2d
.compile externals/misc/_str2int
.compile externals/misc/_meshgrid
.compile externals/misc/_strnumber

.compile proequib_version

